# JS - INGESUP - B2 - Server

## Prerequisites
- [Node][1] 10
- [NPM][2] 6

## Installation

```
npm install
```

## Usage

### Run
```
npm run start
```

### Debug
```
npm run start:debug
```

### Run dist
```
npm run start:dist
```

### Lint
```
npm run lint
```

### Build
```
npm run build
```

### Test
```
npm run test
```

### Endpoints

#### Matches

##### Find all
```
curl -X GET https://js-ingesup-b2.herokuapp.com/matches
```

```
curl -X GET https://js-ingesup-b2.herokuapp.com/matches?sort=homeTeam&size=10&page=1
```

```
curl -X GET https://js-ingesup-b2.herokuapp.com/matches?sort=-homeTeam&size=10&page=1
```

##### Find
```
curl -X GET https://js-ingesup-b2.herokuapp.com/matches/5df049e66409e4428811ffd8
```

##### Create
```
curl -X POST https://js-ingesup-b2.herokuapp.com/matches \
  -H 'Content-Type: application/json' \
  -d '{
        "homeTeam": "Inter Milan",
        "visitorTeam": "Barcelona",
        "score": [1, 2],
        "date": "2019-12-10T21:00:00+01:00"
      }'
}'
```

##### Update
```
curl -X PUT https://js-ingesup-b2.herokuapp.com/matches/5df049e66409e4428811ffd8 \
  -H 'Content-Type: application/json' \
  -d '{
        "id": "5df049e66409e4428811ffd8",
        "homeTeam": "Inter Milan",
        "visitorTeam": "Barcelona",
        "score": [1, 2],
        "date": "2019-12-10T21:00:00+01:00"
      }'
```

##### Delete
```
curl -X DELETE https://js-ingesup-b2.herokuapp.com/matches/5df049e66409e4428811ffd8
```

## Deploy
Via [Heroku][10]

## Major dependencies
- [Express][3]
- [Nodemon][4]
- [Webpack][5]
- [Moment][6]
- [Compression][7]
- [Winston][8]
- [Express Winston][9]

## Author
Alexandre Escudero

[1]: https://nodejs.org/en/download/
[2]: https://www.npmjs.com/get-npm
[3]: http://expressjs.com/en/guide/routing.html
[4]: https://github.com/remy/nodemon
[5]: https://webpack.js.org/concepts
[6]: http://momentjs.com
[7]: https://github.com/expressjs/compression
[8]: https://github.com/winstonjs/winston
[9]: https://github.com/bithavoc/express-winston
[10]: https://devcenter.heroku.com/articles/deploying-nodejs
