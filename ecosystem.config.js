module.exports = {
  apps : [{
    name: 'js-ingesup-b2',
    script: './dist/bundle.js',
    instances: 'max',
    env: {
      NODE_ENV: 'development',
    },
    env_production: {
      NODE_ENV: 'production',
    }
  }]
};
