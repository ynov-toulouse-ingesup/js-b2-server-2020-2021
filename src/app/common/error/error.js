class AppError extends Error {
  constructor(type, message, details) {
    super(message || type && type.description);
    this.type = type;
    this.details = details;
  }
}

class AppErrorType {
  constructor(code, httpStatus, description) {
    this.code = code;
    this.httpStatus = httpStatus;
    this.description = description;
  }
}

const AppErrorTypes = {
  DTO_INVALID_FORMAT: new AppErrorType('DTO_INVALID_FORMAT', 400, 'DTO format is invalid'),
  DTO_INVALID_ID: new AppErrorType('DTO_INVALID_ID', 400, 'DTO id is invalid'),
  PATH_ARAMETER_INVALID_FORMAT: new AppErrorType('PATH_ARAMETER_INVALID_FORMAT', 400, 'Path parameter format is invalid'),
  QUERY_ARAMETER_INVALID_FORMAT: new AppErrorType('QUERY_ARAMETER_INVALID_FORMAT', 400, 'Query parameter format is invalid'),
  RESOURCE_NOT_FOUND: new AppErrorType('RESOURCE_NOT_FOUND', 404, 'Resource has not been found'),
  ENDPOINT_NOT_FOUND: new AppErrorType('ENDPOINT_NOT_FOUND', 404, 'Endpoint has not been found'),
  OTHER_ERROR: new AppErrorType('OTHER_ERROR', 500, 'Other error')
};

module.exports = {
  AppError,
  AppErrorTypes
};
