const moment = require('moment');

const toDto = entity => entity && {
  id: entity.id,
  homeTeam: entity.homeTeam,
  visitorTeam: entity.visitorTeam,
  score: entity.score && entity.score.length ? [...entity.score] : undefined,
  date: moment(entity.date).isValid() ? moment(entity.date).utc().format() : undefined
};

const toEntity = dto => ({
  _id: dto.id,
  homeTeam: dto.homeTeam,
  visitorTeam: dto.visitorTeam,
  score: dto.score && dto.score.length ? [...dto.score] : undefined,
  date: moment.utc(dto.date).isValid() ? moment.utc(dto.date).toDate() : undefined
});

module.exports = {
  toDto,
  toEntity
};
