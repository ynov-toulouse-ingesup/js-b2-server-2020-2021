const Match = require('./match.entity.schema');
const {AppError, AppErrorTypes} = require('../common/error/error');
const {invalidId, notFound} = require('../common/error/mongoose-error.utils');

const findAll = (sort, limit, skip) => Match.find()
  .sort(sort)
  .skip(skip)
  .limit(limit);

const count = () => Match.countDocuments();

const find = id => Match.findById(id)
  .then(entity => {
    if (entity) {
      return entity;
    }
    const error = new AppError(AppErrorTypes.RESOURCE_NOT_FOUND, `No match with id=${id} has been found`);
    return Promise.reject(error);
  })
  .catch(invalidId(id));

const create = entity => {
  const entityToSave = new Match(entity);
  return entityToSave.save();
};

const update = entity => {
  const match = new Match(entity);
  match.isNew = false;
  return match.save()
    .catch(notFound(entity.id))
    .catch(invalidId(entity.id));
};

const remove = id => Match.deleteOne({_id: id})
  .then(response => {
    if (!response.n) {
      return Promise.reject(new AppError(AppErrorTypes.RESOURCE_NOT_FOUND, `No match with id=${id} has been found`));
    }
  })
  .catch(invalidId(id));

module.exports = {
  findAll,
  count,
  find,
  create,
  update,
  remove
};
