const Joi = require('@hapi/joi');
const {AppError, AppErrorTypes} = require('../common/error/error');
const repository = require('./match.repository');
const mapper = require('./match.mapper');
const MatchDto = require('./match.dto.schema');

const joiDtoError = error => Promise.reject(new AppError(AppErrorTypes.DTO_INVALID_FORMAT, undefined,
  error && error.details && error.details[0] && error.details[0].message));

const joiPathParameterError = error => Promise.reject(new AppError(AppErrorTypes.PATH_ARAMETER_INVALID_FORMAT, undefined,
  error && error.details && error.details[0] && error.details[0].message));

const findAll = (req, res, next) => {
  return Promise.resolve()
    .then(() => Promise.all([
      Joi.string()
        .valid(...['homeTeam', 'visitorTeam', 'date'].reduce((acc, field) => [...acc, field, `-${field}`], []))
        .optional()
        .validateAsync(req.query.sort),
      Joi.number().max(50).validateAsync(parseInt(req.query.size) || 10),
      Joi.number().max(1000).validateAsync(parseInt(req.query.page) || 1)
    ]))
    .catch(joiPathParameterError)
    .then(([sort, limit, page]) => [sort, limit, (page - 1) * limit])
    .then(([sort, limit, skip]) => Promise.all([
      repository.findAll(sort, limit, skip),
      repository.count(),
    ]))
    .then(([entities, count]) => [
      (entities || []).map(mapper.toDto),
      count
    ])
    .then(([dtos, count]) => res.header('x-total-count', count).json(dtos))
    .catch(next);
};

const find = (req, res, next) => {
  return Promise.resolve()
    .then(() => req.params.id)
    .then(entityId => Joi.string().required().validateAsync(entityId))
    .catch(joiPathParameterError)
    .then(entityId => repository.find(entityId))
    .then(entity => mapper.toDto(entity))
    .then(dto => res.json(dto))
    .catch(next);
};

const create = (req, res, next) => {
  return Promise.resolve()
    .then(() => req.body)
    .then(dto => Joi.string().max(0).validateAsync(dto.id)
      .then(() => MatchDto.validateAsync(dto)))
    .catch(joiDtoError)
    .then(dto => mapper.toEntity(dto))
    .then(entity => repository.create(entity))
    .then(entity => mapper.toDto(entity))
    .then(dto => res.status(201).json(dto))
    .catch(next);
};

const update = (req, res, next) => {
  return Promise.resolve()
    .then(() => [req.body, req.params.id])
    .then(([dto, entityId]) => {
      return MatchDto.validateAsync(dto)
        .catch(joiDtoError)
        .then(dto => Joi.string().required().validateAsync(entityId)
          .catch(joiPathParameterError)
          .then(entityId => {
            dto.id = entityId;
            return dto;
          }));
    })
    .then(book => mapper.toEntity(book))
    .then(entity => repository.update(entity))
    .then(entity => mapper.toDto(entity))
    .then(dto => res.json(dto))
    .catch(next);
};

const remove = (req, res, next) => {
  return Promise.resolve()
    .then(() => req.params.id)
    .then(entityId => Joi.string().required().validateAsync(entityId))
    .catch(joiPathParameterError)
    .then(entityId => repository.remove(entityId))
    .then(() => res.sendStatus(204))
    .catch(next);
};

module.exports = {
  findAll,
  find,
  create,
  update,
  remove
};
