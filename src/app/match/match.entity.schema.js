const mongoose = require('mongoose');

const schema = mongoose.Schema({
  homeTeam: String,
  visitorTeam: String,
  score: [Number],
  date: Date
});

const model = mongoose.model('Match', schema);
module.exports = model;
